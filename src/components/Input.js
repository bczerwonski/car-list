import styled from "styled-components";
import colors from "../colors";

export default styled.input`
  padding: 1em;
  margin: 8px;
  outline: none;
  min-width: 128px;

  border: none;
  border-bottom: 1px solid ${colors.dark};
  background-color: transparent;

  transition: background-color 0.1s, border-bottom 0.1s;

  &:hover {
    background-color: ${colors.white};
  }

  &:active,
  &:focus {
    background-color: ${colors.white};
    border-bottom-color: ${colors.primary};
  }
`;
