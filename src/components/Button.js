import styled from "styled-components";
import colors from "../colors";

export default styled.button`
  cursor: pointer;
  padding: 1em;
  margin: 8px;
  min-width: 128px;

  font-weight: bold;

  color: ${colors.primary};
  border: 1px solid ${colors.primary};
  background-color: ${colors.dark};

  transition: background-color 0.1s, color 0.1s;

  &:hover {
    background-color: ${colors.primary};
    color: ${colors.white};
  }
`;
