import React, { useState } from "react";
import styled from "styled-components";

import colors from "../../colors";

import { decimalToSexagesimal } from "geolib";

import Button from "../../components/Button";

export default function ListItem({
  plateNumber,
  driver,
  coordinates,
  speed,
  favorite,
  toggleFavorite,
}) {
  const [more, setMore] = useState(false);

  return (
    <Wrapper onClick={() => setMore(!more)}>
      <Favorite onClick={toggleFavorite}>{favorite ? "❤️" : "🤍"}</Favorite>
      <Header>
        Numer rejestracyjny: <b>{plateNumber}</b>
      </Header>
      <Button onClick={() => setMore(!more)}>Więcej...</Button>
      <Details>
        <div>
          Kierowca: <b>{`${driver.name.first} ${driver.name.last}`} </b>
        </div>
        <div>
          Numer kontaktowy: <b>{driver.phoneNumber} </b>
        </div>
        <div>
          Współrzędne geograficzne: {decimalToSexagesimal(coordinates.latitude)}
          <b>N, </b>
          {decimalToSexagesimal(coordinates.longitude)}
          <b>E </b>
        </div>
        <div>
          Średnia prędkość: <b>{speed}</b> km/h
        </div>
      </Details>
    </Wrapper>
  );
}
const Wrapper = styled.div`
  position: relative;
  margin: 1em 0;
  padding: 1em;
  border-right: 2px solid ${colors.dark};
  background-color: ${colors.light};

  transition: border-color 0.1s, background-color 0.1s;

  &:hover {
    border-color: ${colors.primary};
    background-color: ${colors.dark};
  }
`;

const Header = styled.h1`
  font-size: 1.5em;
  margin: 0;
`;

const Details = styled.div`
  display: flex;

  @media only screen and (max-width: 600px) {
    flex-direction: column;
  }
`;

const Favorite = styled.div`
  cursor: pointer;
  font-size: 2em;
  position: absolute;
  right: 1em;
  top: 50%;
  transform: translate(0, -50%);
`;
