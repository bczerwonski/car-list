import { createAsyncThunk, createSlice, current } from "@reduxjs/toolkit";
import {
  getRandomFloatMinMax,
  getRandomInt,
  getRandomLetters,
} from "../../util";
import { computeDestinationPoint } from "geolib";

const initialState = {
  data: [],
  filter: "",
  status: "idle",
};

export const loadCars = createAsyncThunk("cars/fetchCars", async () => {
  const storage = JSON.parse(localStorage.getItem("cars"));

  if (storage) return storage;

  const response = await fetch(`https://randomuser.me/api/?results=5&inc=name`);
  const json = await response.json();
  const driver = json.results;
  const data = driver.map((driver) => {
    return {
      favorite: false,
      plateNumber: `${getRandomLetters(3)}${getRandomInt(100, 999)}`,
      coordinates: {
        latitude: getRandomFloatMinMax(49.0, 54.835778),
        longitude: getRandomFloatMinMax(14.116667, 24.15),
      },
      speed: Math.floor(getRandomFloatMinMax(60, 200)),
      driver: {
        ...driver,
        phoneNumber: `${getRandomInt(100000000, 999999999)}`,
      },
    };
  });
  localStorage.setItem("cars", JSON.stringify(data));
  return data;
});

export const updateCoordinates = createAsyncThunk(
  "cars/updateCoordinates",
  async (arg, { getState }) => {
    const state = getState();

    const timeDelta = 3 / 3600;

    const modifiedCoordinates = state.cars.data.map((car) => {
      return {
        ...car,
        coordinates: computeDestinationPoint(
          car.coordinates,
          timeDelta * car.speed,
          Math.random() % 360
        ),
      };
    });

    localStorage.setItem("cars", JSON.stringify(modifiedCoordinates));
    return modifiedCoordinates;
  }
);

export const carsSlice = createSlice({
  name: "cars",
  initialState,
  reducers: {
    setFilter: (state, action) => {
      state.filter = action.payload;
    },
    toggleFavorite: (state, action) => {
      state.data = current(state.data).map((car, index) => {
        if (car === action.payload)
          return {
            ...car,
            favorite: !car.favorite,
          };
        return car;
      });
      localStorage.setItem("cars", JSON.stringify(state.data));
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadCars.pending, (state) => {
        state.status = "loading";
      })
      .addCase(loadCars.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      })
      .addCase(updateCoordinates.pending, (state) => {
        state.status = "updating";
      })
      .addCase(updateCoordinates.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      });
  },
});

export const { setFilter, toggleFavorite } = carsSlice.actions;

export const selectFilter = ({ cars: { filter } }) => filter;

export const selectData = ({ cars: { data } }) => data;

export const selectFilteredData = ({ cars: { data, filter } }) => {
  if (filter === "") return data;
  return data.filter(
    (car) =>
      car.plateNumber.toLowerCase().includes(filter.toLowerCase()) ||
      car.driver.name.first.toLowerCase().includes(filter.toLowerCase()) ||
      car.driver.name.last.toLowerCase().includes(filter.toLowerCase())
  );
};

export default carsSlice.reducer;
