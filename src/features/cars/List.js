import React from "react";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";

import {
  selectFilteredData,
  selectFilter,
  setFilter,
  toggleFavorite,
} from "./carsSlice";

import Input from "../../components/Input";

import ListItem from "./ListItem";
export default function List() {
  const data = useSelector(selectFilteredData);
  const filter = useSelector(selectFilter);
  const dispatch = useDispatch();

  return (
    <Wrapper>
      <Input
        placeholder="Filter"
        value={filter}
        onChange={(e) => dispatch(setFilter(e.target.value))}
      />
      {data.map((car) => (
        <ListItem
          key={car.plateNumber}
          {...car}
          toggleFavorite={() => dispatch(toggleFavorite(car))}
        />
      ))}
    </Wrapper>
  );
}

const Wrapper = styled.div`
  padding: 1em;
  max-width: 1200px;
  margin-left: auto;
  margin-right: auto;
`;
