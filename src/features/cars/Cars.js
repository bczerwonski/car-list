import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import List from "./List";

import { loadCars, updateCoordinates } from "./carsSlice";

export default function Cars() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadCars());
  }, [dispatch]);

  useEffect(() => {
    const interval = setInterval(() => {
      dispatch(updateCoordinates());
    }, 3000);
    return () => clearInterval(interval);
  }, [dispatch]);

  return <List />;
}
