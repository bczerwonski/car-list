const colors = {
  black: "2f3542",
  white: "#ffffff",
  dark: "#dfe4ea",
  light: "#f1f2f6",
  primary: "#ffa502",
};

export default colors;
